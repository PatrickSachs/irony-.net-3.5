﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Irony.Interpreter")]
[assembly: AssemblyDescription("Irony Interpreter")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Irony Interpreter")]
[assembly: AssemblyCopyright("Copyright © Roman Ivantsov 2011")]
[assembly: AssemblyTrademark("Irony")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f2a09212-b2a0-4315-9e10-d6a5ee614fbe")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

// Required for Sarcasm
[assembly: InternalsVisibleTo("Sarcasm, PublicKey=0024000004800000940000000602000000240000525341310004000001000100a5320b4dd910ecd8cf2abedac077c5a9ee7d26ed2a7797c2090452acc088577092bc841be2410f8ebca131584b38a75a1ec4a986a8667d6d174585b4bc4d94b68a4ecf9eb72b2886c8f1d94c83f9eeb0d307381ea03af0b364791124607dfb91690314397828f50ae338a310f0b2f43543d824d19c486ccaf7409ee8c740e2b0")]
