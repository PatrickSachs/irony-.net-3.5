﻿#region License

/* **********************************************************************************
 * Copyright (c) Roman Ivantsov
 * This source code is subject to terms and conditions of the MIT License
 * for Irony. A copy of the license can be found in the License.txt file
 * at the root of this distribution. 
 * By using this source code in any fashion, you are agreeing to be bound by the terms of the 
 * MIT License.
 * You must not remove this notice from this software.
 * **********************************************************************************/

#endregion

using System;
using System.Linq;
using System.Text;
using Irony.Parsing.Construction;
using PSUtility.Enumerables;

namespace Irony.Parsing
{
    public static class ParserDataPrinter
    {
        public static string PrintStateList(LanguageData language)
        {
            StringBuilder sb = new StringBuilder();
            foreach (ParserState state in language.ParserData.States) {
                sb.Append("State " + state.Name);
                if (state.BuilderData.IsInadequate) {
                    sb.Append(" (Inadequate)");
                }
                sb.AppendLine();
                TerminalSet srConflicts = state.BuilderData.GetShiftReduceConflicts();
                if (srConflicts.Count > 0) {
                    sb.AppendLine("  Shift-reduce conflicts on inputs: " + srConflicts);
                }
                TerminalSet ssConflicts = state.BuilderData.GetReduceReduceConflicts();
                if (ssConflicts.Count > 0) {
                    sb.AppendLine("  Reduce-reduce conflicts on inputs: " + ssConflicts);
                }
                //LRItems
                if (state.BuilderData.ShiftItems.Count > 0) {
                    sb.AppendLine("  Shift items:");
                    foreach (LRItem item in state.BuilderData.ShiftItems) {
                        sb.AppendLine("    " + item);
                    }
                }
                if (state.BuilderData.ReduceItems.Count > 0) {
                    sb.AppendLine("  Reduce items:");
                    foreach (LRItem item in state.BuilderData.ReduceItems) {
                        string sItem = item.ToString();
                        if (item.Lookaheads.Count > 0) {
                            sItem += " [" + item.Lookaheads + "]";
                        }
                        sb.AppendLine("    " + sItem);
                    }
                }
                sb.Append("  Transitions: ");
                bool atFirst = true;
                foreach (BnfTerm key in state.Actions.Keys) {
                    ShiftParserAction action = state.Actions[key] as ShiftParserAction;
                    if (action == null) {
                        continue;
                    }
                    if (!atFirst) {
                        sb.Append(", ");
                    }
                    atFirst = false;
                    sb.Append(key);
                    sb.Append("->");
                    sb.Append(action.NewState.Name);
                }
                sb.AppendLine();
                sb.AppendLine();
            } //foreach
            return sb.ToString();
        }

        public static string PrintTerminals(LanguageData language)
        {
            System.Collections.Generic.List<Terminal> termList = language.GrammarData.Terminals.ToList();
            termList.Sort((x, y) => string.Compare(x.Name, y.Name));
            string result = termList.JoinToString(Environment.NewLine);
            return result;
        }

        public static string PrintNonTerminals(LanguageData language)
        {
            StringBuilder sb = new StringBuilder();
            System.Collections.Generic.List<NonTerminal> ntList = language.GrammarData.NonTerminals.ToList();
            ntList.Sort((x, y) => string.Compare(x.Name, y.Name));
            foreach (NonTerminal nt in ntList) {
                sb.Append(nt.Name);
                sb.Append(nt.Flags.IsSet(TermFlags.IsNullable) ? "  (Nullable) " : string.Empty);
                sb.AppendLine();
                foreach (Production pr in nt.Productions) {
                    sb.Append("   ");
                    sb.AppendLine(pr.ToString());
                }
            } //foreachc nt
            return sb.ToString();
        }
    } //class
} //namespace